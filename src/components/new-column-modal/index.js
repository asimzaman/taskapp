import React from "react";
import { Modal, Input, Button } from "antd";
import PropTypes from "prop-types";

import "./styles.scss";

const NewColumnModal = ({
  colTitle,
  onColTitleChanage,
  onSubmit,
  handleCancel,
}) => {
  return (
    <div>
      <Modal
        visible={true}
        title="Add New Task Column"
        onOk={onSubmit}
        onCancel={handleCancel}
        footer={null}
        wrapClassName={"add-col-modal"}
      >
        <div className="add-col-input">
          <Input
            placeholder="Title"
            value={colTitle}
            onChange={(e) => onColTitleChanage(e.target.value)}
          />
        </div>
        <div className="add-actions">
          <Button className="submit-btn" onClick={onSubmit}>
            Submit
          </Button>
          <Button className="close-btn" onClick={handleCancel}>
            Close
          </Button>
        </div>
      </Modal>
    </div>
  );
};

NewColumnModal.propTypes = {
  colTitle: PropTypes.string,
  onColTitleChanage: PropTypes.func,
  onSubmit: PropTypes.func,
  handleCancel: PropTypes.func,
};
export default NewColumnModal;
