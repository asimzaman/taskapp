import React from "react";
import { Modal, Input, Button } from "antd";
import PropTypes from "prop-types";
import "./styles.scss";

const TaskModal = ({
  taskTitle='',
  onTaskTitleChanage,
  taskDesc='',
  onTaskDescChange,
  onTaskSubmit,
  handleCancel,
}) => {
  return (
    <div>
      <Modal
        visible={true}
        title="Add New Task"
        onCancel={handleCancel}
        footer={null}
        wrapClassName={"add-task-modal"}
      >
        <div className="task-input">
          <Input
            className="input-title"
            placeholder="Title"
            value={taskTitle}
            onChange={(e) => onTaskTitleChanage(e.target.value)}
          />
          <Input
            className="input-description"
            placeholder="Description"
            value={taskDesc}
            onChange={(e) => onTaskDescChange(e.target.value)}
          />
        </div>
        <div className="task-add-actions">
          <Button className="submit-btn" onClick={onTaskSubmit}>
            Submit
          </Button>
          <Button className="close-btn" onClick={handleCancel}>
            Close
          </Button>
        </div>
      </Modal>
    </div>
  );
};
TaskModal.propTypes = {
  addNewColumn: PropTypes.func,
  taskTitle: PropTypes.string,
  onTaskTitleChanage: PropTypes.func,
  taskDesc: PropTypes.string,
  onTaskDescChange: PropTypes.func,
  onTaskSubmit: PropTypes.func,
  handleCancel: PropTypes.func,
};
export default TaskModal;
