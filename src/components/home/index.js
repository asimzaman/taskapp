import React, { useState } from "react";
import AppBar from "../app-bar";
import TaskBoard from "../task-board";
import NewColumnModal from "../new-column-modal";
import TaskModal from "../task-modal";
import { v4 as uuidv4 } from "uuid";
import { taskBoardConfig } from "./data";
import "./styles.scss";

const Home = () => {
  const [taskBoardData, setTaskBoardData] = useState([...taskBoardConfig]);
  const [ordered, setOrdered] = useState([
    ...taskBoardConfig.map((task) => task.id),
  ]);
  const [visibileColModal, setVisibileColModal] = useState(false);
  const [visibleTaskModal, setVisibleTaskModal] = useState(false);
  const [colTitle, setColTitle] = useState("");
  const [taskTitle, setTaskTitle] = useState("");
  const [taskDesc, setTaskDesc] = useState("");
  const [selectedCol, setSelectedCol] = useState("");

  const onColSubmit = () => {
    const taskCol = taskBoardData.find((taskCol) => taskCol.id === selectedCol);
    if (taskCol) {
      const taskBoardDataTemp = taskBoardData.map((taskCol) => {
        if (taskCol.id === selectedCol) {
          taskCol.title = colTitle;
          return taskCol;
        }
        return taskCol;
      });
      setTaskBoardData([...taskBoardDataTemp]);
      setVisibileColModal(false);
      setColTitle("");
      setSelectedCol("");
    } else {
      const newCol = {
        id: `${uuidv4()}`,
        title: colTitle,
        tasks: [],
      };
      setTaskBoardData([...taskBoardData, newCol]);
      setOrdered([...ordered, newCol.id]);
      console.log(taskBoardData);
      setVisibileColModal(false);
      setColTitle("");
    }
  };

  const onTaskSubmit = () => {
    const newTask = {
      id: `${uuidv4()}`,
      title: taskTitle,
      description: taskDesc,
    };
    const taskBoardDataTemp = taskBoardData.map((taskCol) => {
      if (taskCol.id === selectedCol) {
        taskCol.tasks = [...taskCol.tasks, newTask];
        return taskCol;
      }
      return taskCol;
    });
    console.log("taskBoardDataTemp", taskBoardDataTemp);
    setTaskBoardData([...taskBoardDataTemp]);
    setVisibleTaskModal(false);
    setTaskTitle("");
    setTaskDesc("");
  };

  const handleColTitleEdit = (colId) => {
    const taskCol = taskBoardData.find((taskCol) => taskCol.id === colId);
    setSelectedCol(colId);
    setColTitle(taskCol.title);
    setVisibileColModal(true);
  };

  const handleDeleteTaskCol = (colId) => {
    const taskBoardDataTemp = taskBoardData.filter(
      (taskCol) => taskCol.id !== colId
    );
    const taskOrder = ordered.filter((ord) => ord !== colId);
    setTaskBoardData([...taskBoardDataTemp]);
    setOrdered([...taskOrder]);
  };

  return (
    <div className="home-container">
      <AppBar addNewColumn={() => setVisibileColModal(true)} />
      <TaskBoard
        ordered={ordered}
        taskBoardData={taskBoardData}
        handleReorder={(data) => setTaskBoardData(data)}
        handleOrdering={(ordered) => setOrdered(ordered)}
        onAddTask={(colId) => {
          setSelectedCol(colId);
          setVisibleTaskModal(true);
        }}
        onEditColTitle={(colId) => handleColTitleEdit(colId)}
        onDeleteTaskCol={(colId) => handleDeleteTaskCol(colId)}
      />
      {visibileColModal && (
        <NewColumnModal
          colTitle={colTitle}
          onColTitleChanage={(value) => setColTitle(value)}
          onSubmit={onColSubmit}
          handleCancel={() => {
            setVisibileColModal(false);
            setColTitle("");
          }}
        />
      )}
      {visibleTaskModal && (
        <TaskModal
          taskTitle={taskTitle}
          taskDesc={taskDesc}
          onTaskTitleChanage={(value) => setTaskTitle(value)}
          onTaskDescChange={(value) => setTaskDesc(value)}
          onTaskSubmit={onTaskSubmit}
          handleCancel={() => {
            setVisibleTaskModal(false);
            setTaskTitle("");
            setTaskDesc("");
          }}
        />
      )}
    </div>
  );
};
export default Home;
