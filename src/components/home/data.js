export const taskBoardConfig = [
  {
    id: "1",
    title: "Not  Started",
    tasks: [
      {
        id: "001",
        title: "Task-1",
        description: "Web design",
      },
      {
        id: "002",
        title: "Task-2",
        description:
          "SEO",
      },
    ],
  },
  {
    id: "2",
    title: "Pending",
    tasks: [],
  },

  {
    id: "3",
    title: "In Progress",
    tasks: [],
  },
  {
    id: "4",
    title: "Completed",
    tasks: [],
  },
];
