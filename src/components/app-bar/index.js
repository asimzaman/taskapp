import React from "react";
import { Button, Typography } from "antd";
import { IoMdAdd } from "react-icons/io";
import PropTypes from "prop-types";
import "./styles.scss";

const { Title } = Typography;
const AppBar = ({ addNewColumn }) => {
  return (
    <div className="app-bar">
      <div className="app-bar-title">
        <Title level={4}>TASKRR...</Title>
      </div>

      <div className="add-col-btn" onClick={addNewColumn}>
        <Button size={"large"}>
          <div>
            <IoMdAdd />
          </div>{" "}
          <div style={{ marginLeft: "3px" }}>Add New Column</div>
        </Button>
      </div>
    </div>
  );
};

AppBar.propTypes = {
  addNewColumn: PropTypes.func,
};
export default AppBar;
