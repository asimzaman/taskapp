import React from "react";
import { Draggable } from "react-beautiful-dnd";
import { Typography } from 'antd';
import PropTypes from "prop-types";
import TaskList from "../task-list";
import { GrDrag } from "react-icons/gr";
import { FaRegEdit } from "react-icons/fa";
import { RiDeleteBin6Line } from "react-icons/ri";


import "./styles.scss";
const { Title } = Typography;
const Column = ({
  title  = '',
  colId = '',
  taskBoardData = [],
  index,
  isScrollable,
  isCombineEnabled,
  onAddTask,
  onEditColTitle,
  onDeleteTaskCol
}) => {
  return (
    <Draggable draggableId={colId} index={index}>
      {(provided, snapshot) => (
        <div
          className="container"
          ref={provided.innerRef}
          {...provided.draggableProps}
        >
          <div
            className={`header ${snapshot.isDragging && "isDragging"}`}
            isDragging={snapshot.isDragging}
          >
            <div className="header-left">
              <div
                {...provided.dragHandleProps}
                isDragging={snapshot.isDragging}
              >
                <GrDrag />
              </div>
              
              <div className="title">
                  <Title level={4} ellipsis={{rows: 1}}>{title}</Title>
              </div>
              </div>
            <div className="header-right">
              <div  className="edit-icon" onClick={() => onEditColTitle(colId)}><FaRegEdit /></div>
              <div className="delete-icon" onClick={() => onDeleteTaskCol(colId)}><RiDeleteBin6Line /></div>
            </div>
          </div>
          <TaskList
            colId={colId}
            listId={title}
            listType="TASK"
            taskBoardData={taskBoardData}
            internalScroll={isScrollable}
            isCombineEnabled={Boolean(isCombineEnabled)}
            onAddTask={onAddTask}
          />
        </div>
      )}
    </Draggable>
  );
};
Column.propTypes = {
  addNewColumn: PropTypes.func,
  title:PropTypes.string,
  colId: PropTypes.func,
  taskBoardData : PropTypes.array,
  index: PropTypes.number,
  isScrollable: PropTypes.bool,
  isCombineEnabled: PropTypes.bool,
  onAddTask: PropTypes.func,
  onEditColTitle: PropTypes.func,
  onDeleteTaskCol: PropTypes.func,
};
export default Column;
