import React from "react";
import Column from "./column";
import reorder, { reorderQuoteMap } from "./reorder";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import PropTypes from "prop-types";
import "./styles.scss";


const TaskBoard = ({
  taskBoardData = [],
  ordered = [],
  handleOrdering,
  containerHeight,
  handleReorder,
  isCombineEnabled,
  withScrollableColumns,
  onAddTask,
  onEditColTitle,
  onDeleteTaskCol
}) => {

  const onDragEnd = (result) => {
    // dropped nowhere
    if (!result.destination) {
      return;
    }

    const source = result.source;
    const destination = result.destination;

    // did not move anywhere - can bail early
    if (
      source.droppableId === destination.droppableId &&
      source.index === destination.index
    ) {
      return;
    }

    // reordering column
    if (result.type === "COLUMN") {
      const ordereded = reorder(ordered, source.index, destination.index);
      handleOrdering(ordereded)
      // setOrdered(ordereded);

      return;
    }

    const data = reorderQuoteMap({
      quoteMap: taskBoardData,
      source,
      destination,
    });

    handleReorder(data.quoteMap);
  };
  const board = () => {
    return (
      <Droppable
        droppableId="board"
        type="COLUMN"
        direction="horizontal"
        ignoreContainerClipping={Boolean(containerHeight)}
        isCombineEnabled={isCombineEnabled}
      >
        {(provided) => (
          <div className='task-board' ref={provided.innerRef} {...provided.droppableProps}>
            {ordered.map((id, index) => {
             const taskCol = taskBoardData.find(taskCol => (taskCol || {}).id === id)
             return <Column
                key={id}
                colId={(taskCol || {}).id}
                index={index}
                title={(taskCol || {}).title}
                taskBoardData={(taskCol || {})}
                isScrollable={withScrollableColumns}
                isCombineEnabled={true}
                onAddTask={onAddTask}
                onEditColTitle={onEditColTitle}
                onDeleteTaskCol={onDeleteTaskCol}
              />
              })}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    );
  };

  return (
    <React.Fragment>
      <DragDropContext onDragEnd={onDragEnd}>
        {containerHeight ? (
          <div className="taskboard-container" height={containerHeight}>{board()}</div>
        ) : (
          board()
        )}
      </DragDropContext>
    </React.Fragment>
  );
};

TaskBoard.propTypes = {
  addNewColumn: PropTypes.func,
  taskBoardData: PropTypes.array,
  ordered:PropTypes.array,
  handleOrdering:  PropTypes.func,
  containerHeight:  PropTypes.any,
  handleReorder:  PropTypes.func,
  isCombineEnabled:  PropTypes.bool,
  withScrollableColumns:  PropTypes.bool,
  onAddTask:  PropTypes.func,
  onEditColTitle :  PropTypes.func,
  onDeleteTaskCol:  PropTypes.func,
};
export default TaskBoard;
