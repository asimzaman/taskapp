const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

export default reorder;

export const reorderQuoteMap = ({ quoteMap, source, destination }) => {
  const currentTask = quoteMap.find((col) => col.title === source.droppableId);
  const currentTasks = (currentTask || {}).tasks;
  const current = [...currentTasks];

  if (source.droppableId === destination.droppableId) {
    const reordered = reorder(current, source.index, destination.index);
    const result = quoteMap.map((qt) => {
      if (qt.title === source.droppableId) {
        qt.tasks = reordered;
      }
      return qt;
    });
    return {
      quoteMap: result,
    };
  } else {
    return {
      quoteMap,
    };
  }
};
