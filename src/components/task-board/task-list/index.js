import React from "react";
import { Droppable, Draggable } from "react-beautiful-dnd";
import { Button } from "antd";
import TaskItem from "../task-item";
import PropTypes from "prop-types";
import "./styles.scss";

const InnerTaskList = ({ tasks }) => {
  return (
    tasks &&
    tasks.length > 0 &&
    tasks.map((task, index) => (
      <Draggable
        key={(task || {}).id}
        draggableId={(task || {}).id}
        index={index}
        shouldRespectForceTouch={false}
      >
        {(dragProvided, dragSnapshot) => (
          <TaskItem
            key={(task || {}).id}
            task={task || {}}
            isDragging={dragSnapshot.isDragging}
            isGroupedOver={Boolean(dragSnapshot.combineTargetFor)}
            provided={dragProvided}
          />
        )}
      </Draggable>
    ))
  );
};

const InnerList = (props) => {
  const { tasks, dropProvided } = props;
  // const title = props.title ? <div className="title">{props.title}</div> : null;

  return (
    <div>
      {/* {title} */}
      <div className="drop-zone" ref={dropProvided.innerRef}>
        <InnerTaskList tasks={tasks && tasks} />
        {dropProvided.placeholder}
      </div>
    </div>
  );
};

const TaskList = (props) => {
  const {
    ignoreContainerClipping,
    internalScroll,
    isDropDisabled,
    isCombineEnabled,
    listId = "LIST",
    listType,
    taskBoardData,
    title,
    onAddTask,
    colId,
  } = props;
  const tasks = (taskBoardData || []).tasks;
  return (
    <Droppable
      droppableId={listId}
      type={listType}
      ignoreContainerClipping={ignoreContainerClipping}
      isDropDisabled={isDropDisabled}
      isCombineEnabled={isCombineEnabled}
    >
      {(dropProvided, dropSnapshot) => (
        <div
          className={"task-list-wrapper"}
          isDraggingOver={dropSnapshot.isDraggingOver}
          isDropDisabled={isDropDisabled}
          isDraggingFrom={Boolean(dropSnapshot.draggingFromThisWith)}
          {...dropProvided.droppableProps}
        >
          {internalScroll ? (
            <div className="scroll-container">
              <InnerList
                tasks={tasks}
                title={title}
                dropProvided={dropProvided}
              />
            </div>
          ) : (
            <InnerList
              tasks={tasks}
              title={title}
              dropProvided={dropProvided}
            />
          )}
          <div className="add-task-btn">
            <Button size={"small"} onClick={() => onAddTask(colId)}>
              Add Task
            </Button>
          </div>
        </div>
      )}
    </Droppable>
  );
};

TaskList.propTypes = {
  addNewColumn: PropTypes.func,
  ignoreContainerClipping: PropTypes.bool,
  internalScroll: PropTypes.bool,
  isDropDisabled: PropTypes.bool,
  isCombineEnabled: PropTypes.bool,
  listId: PropTypes.string,
  listType: PropTypes.string,
  taskBoardData: PropTypes.array,
  title: PropTypes.string,
  onAddTask: PropTypes.func,
  colId: PropTypes.string,
};
export default TaskList;
