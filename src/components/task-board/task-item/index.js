import React from "react";
import { GrDrag } from "react-icons/gr";
import { Typography } from 'antd';
import PropTypes from "prop-types";
import './styles.scss';


const { Title, Paragraph } = Typography;


const TaskItem = ({ task, provided }) => {
    return (
      <div
        className={'task-container'}
        // href={quote.author.url}
        // isDragging={isDragging}
        // isGroupedOver={isGroupedOver}
        // colors={quote.author.colors}
        ref={provided.innerRef}
        {...provided.draggableProps}
        // {...provided.dragHandleProps}
      >
        <div className="task-drag" {...provided.dragHandleProps}>
           <GrDrag />
        </div>
        <div className='task-content'>
          <div className='content-title'><Title ellipsis={{ rows: 1}} level={5}>{task.title}</Title></div>
          <div className='content'><Paragraph ellipsis={{ rows: 1}}>{task.description}</Paragraph></div>
        </div>
      </div>
    );
}
TaskItem.propTypes = {
  task: PropTypes.object,
  provided: PropTypes.any
};
export default TaskItem
