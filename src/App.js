import Home from './components/home'
import 'antd/dist/antd.css';
import './App.css';

function App() {
  return (
    <div className="App">
       <Home />
    </div>
  );
}

export default App;
